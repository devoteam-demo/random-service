package be.devoteam.demo.service.randomexceptionservice.controller;

import be.devoteam.demo.service.randomexceptionservice.service.RandomService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/random")
public class RandomController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private RandomService randomService;

    @RequestMapping(method = RequestMethod.GET)
    public void logRequest(){
        logger.info("Request logged");
        randomService.process();
    }

}
