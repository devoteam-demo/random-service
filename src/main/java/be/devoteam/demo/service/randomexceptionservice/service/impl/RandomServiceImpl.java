package be.devoteam.demo.service.randomexceptionservice.service.impl;

import be.devoteam.demo.service.randomexceptionservice.client.RandomFeignClient;
import be.devoteam.demo.service.randomexceptionservice.service.RandomService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Random;

@Service
public class RandomServiceImpl implements RandomService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${call.other.service:false}")
    private Boolean callService;

    @Resource
    private RandomFeignClient randomFeignClient;

    @Override
    public void process() {
        logger.info("Executing process for randomservice");
        Random r = new Random();
        if(r.nextInt(10) == 5){
            throw new RuntimeException("Oops");
        } else if(callService) {
            try{
                randomFeignClient.callRandomService();
            } catch (Exception e) {
                logger.error("An exception occured", e);
            }
        }
        logger.info("Finished processing");
    }
}
