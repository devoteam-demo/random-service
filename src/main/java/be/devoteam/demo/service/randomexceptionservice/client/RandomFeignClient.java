package be.devoteam.demo.service.randomexceptionservice.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(url = "${service.address}", name = "${service.name}")
public interface RandomFeignClient {

    @RequestMapping(value = "${service.endpoint}", method = RequestMethod.GET)
    void callRandomService();

}
